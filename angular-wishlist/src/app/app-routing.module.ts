import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { DestinoDetalleComponent } from './pages/destino-detalle/destino-detalle.component';
import { ListaDestinosComponent } from './pages/lista-destinos/lista-destinos.component';
import { LoginComponent } from './pages/login/login.component';
import { ProtectedComponent } from './pages/protected/protected.component';
import { VuelosComponentComponent } from './pages/vuelos/vuelos-component/vuelos-component.component';
import { VuelosDetalleComponent } from './pages/vuelos/vuelos-detalle/vuelos-detalle.component';
import { VuelosMainComponent } from './pages/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './pages/vuelos/vuelos-mas-info/vuelos-mas-info.component';

export const childrenRoutersVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent },
  { path: 'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent }
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  { path: 'protected', component: ProtectedComponent, canActivate: [UsuarioLogueadoGuard] },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutersVuelos
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
