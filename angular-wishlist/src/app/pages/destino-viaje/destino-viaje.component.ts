import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DeleteDestinoAction, voteDownAction, voteUpAction } from 'src/app/models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() position: number;
  /*vinculacion directa a un atributo (se le pone la class al div que se genera al renderear)*/
  @HostBinding('attr.class') cssClass = 'col-md-4';

  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();

  }

  ngOnInit(): void {
  }

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new voteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new voteDownAction(this.destino));
    return false;
  }

  delete(){
    this.store.dispatch( new DeleteDestinoAction(this.destino));
    return false;
  }

}
